import React from "react";
import styled from "@emotion/styled";
import PropTypes from "prop-types";

const FipFlag = styled("path")`
  fill: ${props => props.fillColor};
`;

const FipText = styled("path")`
  fill: ${props => props.text};
  transform: ${props => (props.transform ? props.transform : "translate(0 0)")};
`;

const GOCSymbol = ({
  width = "300px",
  fillColor = props.fillColor,
  lang = props.lang,
  ...props
}) => (
  <svg
    role="img"
    aria-label={lang === "en" ? "Government of Kenya" : "Gouvernement du Kenya"}
    xmlns="http://www.w3.org/2000/svg"
    version="1.1"
    width={width}
    viewBox="0 0 819 75.97"
    preserveAspectRatio="xMinYMin meet"
    shapeRendering="geometricPrecision"
    {...props}
  >
    <g id="sig" transform="translate(0 -0.02)">
      <h1>Kenya</h1>
    </g>
  </svg>
);

GOCSymbol.propTypes = {
  width: PropTypes.string,
  fillColor: PropTypes.string,
  lang: PropTypes.string
};

export default GOCSymbol;
