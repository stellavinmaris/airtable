import React from "react";
import styled from "@emotion/styled";
import PropTypes from "prop-types";

const FipFlag = styled.path`
  fill: ${props => props.flag};
`;

const FipText = styled.path`
  fill: ${props => props.text};
`;

const Wordmark = ({
  width = "10em",
  flag = "#F00",
  text = "#000",
  lang = "en",
  ...props
}) => <p></p>;

Wordmark.propTypes = {
  width: PropTypes.string,
  flag: PropTypes.string,
  text: PropTypes.string,
  lang: PropTypes.string
};

export default Wordmark;
